package dfroot87.bitbucket.aircraftqueue.model.interfaces;

/**
 * Created by phthomas on 4/26/2015.
 */
public interface Aircraft {
    public static final int TYPE_PASSENGER = 0;
    public static final int TYPE_CARGO = 1;
    public static final int SIZE_SMALL = 0;
    public static final int SIZE_LARGE = 1;

    public int getType();
    public void setType(int type);

    public int getSize();
    public void setSize(int size);

    public String getDisplayName();

    public Integer getQueuePrecedence();
}
