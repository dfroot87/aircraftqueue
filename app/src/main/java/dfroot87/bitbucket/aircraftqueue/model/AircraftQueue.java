package dfroot87.bitbucket.aircraftqueue.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import dfroot87.bitbucket.aircraftqueue.model.interfaces.Aircraft;

/**
 * Created by phthomas on 4/26/2015.
 */
public class AircraftQueue {

    private Map queueMap;
    private int queueSize;

    public AircraftQueue() {
        queueMap = new HashMap<Integer, List<Aircraft>>();
        queueSize = 0;
    }

    //Initialize the map and our Lists (one for each known precedence)
    public void bootSystem() {
        queueMap.put(new Integer(1), new ArrayList<Aircraft>());
        queueMap.put(new Integer(2), new ArrayList<Aircraft>());
        queueMap.put(new Integer(3), new ArrayList<Aircraft>());
        queueMap.put(new Integer(4), new ArrayList<Aircraft>());
    }

    //Insert an aircraft into the proper queue list
    public void enqueueAircraft(Aircraft aircraft) {

        //Get the aircraft list that matches with the passed-in aircraft's precedence
        List subQueue = (ArrayList<Aircraft>) queueMap.get(aircraft.getQueuePrecedence());

        //add it to the queue and increment the size
        subQueue.add(aircraft);
        queueSize++;
    }

    public Aircraft dequeueAircraft() {
        List subQueue;

        //traverse the map in order of precedence
        for (int i = 1; i <= 4; i++) {

            //pull the list out of the map based on precedence
            subQueue = (ArrayList<Aircraft>) queueMap.get(new Integer(i));

            //If there is something in the list, it needs to be removed and returned to the caller
            if (!subQueue.isEmpty()) {
                queueSize--;
                return (Aircraft)subQueue.remove(0);
            }
        }

        return null;
    }

    public int getQueueSize() {
        return queueSize;
    }
}
