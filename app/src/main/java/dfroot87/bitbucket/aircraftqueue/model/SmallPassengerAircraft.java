package dfroot87.bitbucket.aircraftqueue.model;

import dfroot87.bitbucket.aircraftqueue.model.interfaces.Aircraft;

/**
 * Created by phthomas on 4/26/2015.
 */
public class SmallPassengerAircraft implements Aircraft {

    private Integer queuePrecedence;
    private int type;
    private int size;

    public SmallPassengerAircraft() {
        queuePrecedence = new Integer(2);
        type = Aircraft.TYPE_CARGO;
        size = Aircraft.SIZE_LARGE;
    }

    @Override
    public String getDisplayName() {
        return "Small Passenger";
    }

    @Override
    public int getType() {
        return type;
    }

    @Override
    public void setType(int type) {
        this.type = type;
    }

    @Override
    public int getSize() {
        return size;
    }

    @Override
    public void setSize(int size) {
        this.size = size;
    }

    @Override
    public Integer getQueuePrecedence() {
        return queuePrecedence;
    }
}
