package dfroot87.bitbucket.aircraftqueue.activities;

import android.animation.ObjectAnimator;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.os.Build;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.TextView;

import dfroot87.bitbucket.aircraftqueue.R;
import dfroot87.bitbucket.aircraftqueue.model.AircraftQueue;
import dfroot87.bitbucket.aircraftqueue.model.interfaces.Aircraft;
import dfroot87.bitbucket.aircraftqueue.util.AircraftFactory;


public class MainActivity extends ActionBarActivity {
    private TextView mQueueMessage;
    private Button mAddButton;
    private Button mRemoveButton;
    private Button mSaveButton;
    private Aircraft mLastDequeuedAircraft;
    private AircraftQueue mAircraftQueue;

    private int pendingType = Aircraft.TYPE_CARGO;
    private int pendingSize = Aircraft.SIZE_LARGE;

    private CardView mCardAddView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Initialize toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle("Aircraft Queue Manager");

        //get reference to all views
        mQueueMessage = (TextView)findViewById(R.id.txt_queue_size);
        mAddButton = (Button) findViewById(R.id.btn_add_aircraft);
        mRemoveButton = (Button) findViewById(R.id.btn_remove_aircraft);
        mSaveButton = (Button) findViewById(R.id.btn_save_aircraft);

        mCardAddView = (CardView) findViewById(R.id.card_add);

        //initialize the aircraft queue
        mAircraftQueue = new AircraftQueue();
        mAircraftQueue.bootSystem();

        //initialize listeners
        initListeners();
    }

    private void initListeners() {
        mAddButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //display card form
                mCardAddView.setVisibility(View.VISIBLE);
            }
        });

        mRemoveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //if there are planes in the view
                if(mAircraftQueue.getQueueSize() > 0) {
                    //dequeue the next plane and get a reference to it
                    mLastDequeuedAircraft = mAircraftQueue.dequeueAircraft();

                    //update the message with plane details
                    mQueueMessage.setText(mLastDequeuedAircraft.getDisplayName() + " Removed");
                }
                else {
                    //update the message to show that it is empty
                    mQueueMessage.setText(getResources().getText(R.string.empty_queue));
                }
                //perform a shake animation to show updates to the text
                ObjectAnimator
                        .ofFloat(mQueueMessage, "translationX", 0, 25, -25, 25, -25, 15, -15, 6, -6, 0)
                        .setDuration(200)
                        .start();
            }
        });

        mSaveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //add the new aircraft to the proper place in the queue
                mAircraftQueue.enqueueAircraft(AircraftFactory.getAircraft(pendingType, pendingSize));
                mQueueMessage.setText(mAircraftQueue.getQueueSize() + " Aircrafts in Queue");
                mCardAddView.setVisibility(View.INVISIBLE);
            }
        });
    }

    //This method listens for the Size radio buttons selection
    public void onSizeRadioButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.radio_large:
                if (checked)
                    pendingSize = Aircraft.SIZE_LARGE;
                    break;
            case R.id.radio_small:
                if (checked)
                    pendingSize = Aircraft.SIZE_SMALL;
                    break;
        }
    }

    //This method listens for the Type radio button selection
    public void onTypeRadioButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.radio_cargo:
                if (checked)
                    pendingType = Aircraft.TYPE_CARGO;
                    break;
            case R.id.radio_passenger:
                if (checked)
                    pendingType = Aircraft.TYPE_PASSENGER;
                    break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
