package dfroot87.bitbucket.aircraftqueue.util;

import dfroot87.bitbucket.aircraftqueue.model.LargeCargoAircraft;
import dfroot87.bitbucket.aircraftqueue.model.LargePassengerAircraft;
import dfroot87.bitbucket.aircraftqueue.model.SmallCargoAircraft;
import dfroot87.bitbucket.aircraftqueue.model.SmallPassengerAircraft;
import dfroot87.bitbucket.aircraftqueue.model.interfaces.Aircraft;

/**
 * Created by phthomas on 4/26/2015.
 */
public class AircraftFactory {

    public static Aircraft getAircraft(int type, int size) {
        if (type == Aircraft.TYPE_PASSENGER) {
            if (size == Aircraft.SIZE_SMALL) {
                return new SmallPassengerAircraft();
            }
            else if (size == Aircraft.SIZE_LARGE) {
                return new LargePassengerAircraft();
            }
        }
        else if (type == Aircraft.TYPE_CARGO) {
            if (size == Aircraft.SIZE_SMALL) {
                return new SmallCargoAircraft();
            }
            else if (size == Aircraft.SIZE_LARGE) {
                return new LargeCargoAircraft();
            }
        }

        return null;
    }
}
